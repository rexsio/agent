from agent.rabbitmq.consumer import RabbitMqConsumer
from agent.rabbitmq.publisher import RabbitMQPublisher

rabbitmq_consumer = RabbitMqConsumer()
rabbitmq_publisher = RabbitMQPublisher()
