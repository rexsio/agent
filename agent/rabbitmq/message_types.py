from typing import NamedTuple


class DeclareQueueMessage(NamedTuple):
    topic: str
